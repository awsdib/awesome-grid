import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import App from './App';
import Adapter from 'enzyme-adapter-react-16'

configure({ adapter: new Adapter() });

describe('App component testing', function() {
  it('renders grid view', function() {
    const wrapper = shallow(<App />); 
    const table = <table class="table dx-g-bs4-table" style="min-width: 900px;"></table>;
    expect(wrapper.find(table)).to.equal(true);
  });
});