import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import { 
  Grid, 
  DragDropProvider,
  Table, 
  TableHeaderRow,
  ColumnChooser,
  TableColumnVisibility,
  Toolbar,
  PagingPanel, 
  TableColumnReordering,
  TableFilterRow,
  TableColumnResizing, 
} from '@devexpress/dx-react-grid-bootstrap4';

import { 
  SortingState,
  IntegratedSorting,
  PagingState, 
  IntegratedPaging,
  FilteringState,
  IntegratedFiltering,
} from '@devexpress/dx-react-grid';

import customers from './data_all';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      columns: [
        { name: 'id', title: 'id' },
        { name: 'sex', title: 'sex' },
        { name: 'firstName', title: 'firstName' },
        { name: 'lastName', title: 'lastName' },
        { name: 'city', title: 'city' },
        { name: 'wage', title: 'wage' },
        { name: 'phone', title: 'phone' },
        { name: 'date', title: 'date' },
        { name: 'children', title: 'children' },
      ],
      defaultColumnWidths: [
        { columnName: 'id', width: 180 },
        { columnName: 'sex', width: 180 },
        { columnName: 'firstName', width: 180 },
        { columnName: 'lastName', width: 180 },
        { columnName: 'city', width: 180 },
        { columnName: 'wage', width: 180 },
        { columnName: 'phone', width: 180 },
        { columnName: 'date', width: 180 },
        { columnName: 'children', width: 180 },
      ],
      tableColumnExtensions: [
        { columnName: 'id', width: 180 },
        { columnName: 'sex', width: 180 },
        { columnName: 'firstName', width: 180 },
        { columnName: 'lastName', width: 180 },
        { columnName: 'city', width: 180 },
        { columnName: 'wage', width: 180 },
        { columnName: 'phone', width: 180 },
        { columnName: 'date', width: 180 },
        { columnName: 'children', width: 180 },
      ],
      rows: customers,
      pageSizes: [5, 20, 100, 1000],
      defaultHiddenColumnNames: ['wage', 'phone', 'date', 'children'],
    };

    this.changeColumnOrder = (newOrder) => {
      this.setState({ columnOrder: newOrder });
    };
  }

  render() {
    const { rows, columns, pageSizes, defaultColumnWidths, tableColumnExtensions, defaultHiddenColumnNames } = this.state;

    return (
      <div className="App">
        <Grid
          rows={ rows }
          columns={ columns }
        >
          <PagingState 
            defaultCurrentPage={0}
            defaultPageSize={3}
          />
          <SortingState
            defaultSorting={[{ columnName: 'id', direction: 'asc' }]}
          />
          <FilteringState defaultFilters={[]} />

          <IntegratedPaging />
          <IntegratedSorting />
          <IntegratedFiltering />

          <DragDropProvider />

          <Table
             columnExtensions={tableColumnExtensions}
          />
                    
          <TableColumnReordering
            defaultOrder={['id', 'sex', 'firstName', 'lastName', 'city', 'wage', 'phone', 'date', 'children']}
          />
          
          <TableColumnResizing defaultColumnWidths={defaultColumnWidths} />
          
          <TableHeaderRow showSortingControls />
          <TableColumnVisibility
            defaultHiddenColumnNames={defaultHiddenColumnNames}
          />
          <Toolbar />
          <ColumnChooser />

          <TableFilterRow />

          <PagingPanel
            pageSizes={pageSizes}
          />
        </Grid>
      </div>
    );
  }
}

export default App;
