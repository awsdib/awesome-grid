Create a react app from scratch and render a data table with 100k (from data.json) rows with the following features:
  - Must-have:
    - Sorting
    - Pagination
    - Filtering/searching
  - Nice-to-have
    - Resizable columns
    - Column visibility options (toggles to show/hide specific columns)
    - Reorderable columns


The data comes in the following format:
[
  {
    "id":0
    "sex":"Female",
    "firstName":"Sandra",
    "lastName":"Moore",
    "city":"London",
    "wage":3325,
    "phone":"(890) 605-6316",
    "date":"2016-11-28",
    "children":2
  },
  {...},
  {...}
]

Bonus task:
Unit tests for any features implemented.

Use whatever tools you see fit for the task.

Good luck! :)

